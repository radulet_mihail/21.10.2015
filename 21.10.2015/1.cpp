// Se citeste x intreg. Sa se calculeze ultima cifra a lui 2x

#include <iostream>
using namespace std;
int main() {
	int x;

	cin >> x;

	cout << "lastDigit(2^" << x << ")=";

	if (x == 0) {
		cout << 1;
	}
	else {
		switch (x % 4) {
		case 1: cout << 2; break;
		case 2: cout << 4; break;
		case 3: cout << 8; break;
		case 0: cout << 6; break;
		}
	}
}